#include "funktionen.h"
#include <iostream>
//--------------------------------------------------------------------------
// Print Funktion mit fester Anzahl an Elementen
ostream& print_array_10(ostream& os, int* a) {

   // os << endl << a[0] << " " << endl;
    os << endl << endl << "print_array_10 Funktion: \n";
    for (int i{}; i < 10; ++i) {
        os << a[i] << " ";
    }

    return os;

//--------------------------------------------------------------------------  
// Print Funktion mit übergebener Anzahl an Elementen
}
ostream& print_array(ostream& os, int* a, const int& anzahl) {
    os << endl << endl << "print_array Funktion: \n";
    for (int i{}; i < anzahl; ++i) {
        os << a[i] << " ";
    }
    return os;
}