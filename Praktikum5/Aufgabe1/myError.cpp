#include "myError.h"

#include <stdexcept>


void error(const string& s) { throw std::runtime_error{ s }; }
void error(const string& s1, const string& s2) { error(s1 + s2); }
