/*
Pierre Baron 03.07.2020
PAD1 Praktikum 5
Aufgabe 1
In dem Programm wird mit Pointer und new experimentiert.
Es wird eine Funktion erstellt, die ein Datenfeld auf welches ein Pointer weist ausgeben kann.
*/

#include "myError.h"
#include <iostream>
#include "funktionen.h"
using std::cout;
using std::endl;




int main()try
{
//--------------------------------------------------------------------------
    //a)
    double* pd1{ new double{9.8765} };
    cout << "Wert von pd1: " << pd1 << endl;
    cout << "Deref von pd1: " << *pd1 << endl << endl;
    delete pd1;
    pd1 = nullptr;

//--------------------------------------------------------------------------
    //b)
    const int anzahl{ 10 };
    int* a{ new int[anzahl]};
    cout << "Die Werte vom Datenfeld a sind:\n";
    for (int i{}; i < anzahl; ++i) {
        cout << a[i] << " ";
    }
    
//--------------------------------------------------------------------------
    //c)
    print_array_10(cout, a);
    
//--------------------------------------------------------------------------
    //d)
    //Wenn a auf weniger als 10 int weist, dann werden die n�chsten "garbage" Zahlen (bis 10) ausgegeben
    //Wenn a auf mehr als 10 int weist, werden trotzdem nur die 10 Zahlen ausgegeben.
    //L�sung: Size �bergeben.


    print_array(cout, a, anzahl);

    delete[] a;
    a = nullptr;

    return 0;
}




catch (std::exception& e)
{
    std::cerr << "Fehler: " << e.what();
    return -2;
}
catch (...)
{
    std::cerr << "Ein Fehler";
    return -1;
}