#pragma once
#include <iostream>
#include "myError.h"
using std::ostream;
using std::endl;

//--------------------------------------------------------------------------
// Die beiden Print Funktionen
ostream& print_array_10(ostream& os, int* a);
ostream& print_array(ostream& os, int* a, const int &anzahl);