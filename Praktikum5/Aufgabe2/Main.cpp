/*
Pierre Baron 03.07.2020
PAD 1 Praktikum 5
Aufgabe 2
C-Strings sollen ohne STD Funktionen kopiert werden und 
Gro�buchstaben in C-Strings sollen zu Kleinbuchstaben gemacht werden.
*/
#include "myError.h"
#include <iostream>
#include "function.h"

using std::cout;
using std::endl;








int main()try
{
// Diesen C-String beim testen �ndern


    char* cs{ new char[14]{"Hallo Mohamed"} }; 
    //char* cs{ new char[17]{"kleinebuchstaben"} };
    //char* cs{ new char[5]{"\n\n\n1"} };
    //char* cs{ new char[9]{"12341234"} };
    //char* cs{ new char[11]{"ALLESGROSS"} };
    //char* cs{ new char[2]{"�"} };


//-----------------------------------------------------------------------------------------------
    cout << "Der urspuengliche C-String lautet: " << endl;
    for (size_t i{0}; i < 13; ++i) {
        cout << cs[i];
    }
    tolower(cs);

    cout << endl << endl;
    cout << "Der C-String nach tolower() lautet: " << endl;
    for (size_t i{}; i < 13; ++i) {
        cout << cs[i];
    }

//------------------------------------------------------------------------------------------------
    char* copy{ cstring_copier(cs) };
    cout << "\n\nDer kopierte C-String lautet: " << endl;

    for (size_t i{}; i < 13; ++i) {
        cout << copy[i];
    }
    cout << endl;

    // Speicher freigeben
    delete[] copy;
    copy = nullptr;
    delete[] cs;
    cs = nullptr;

    return 0;


}




catch (std::exception& e)
{
    std::cerr << "Fehler: " << e.what();
    return -2;
}
catch (...)
{
    std::cerr << "Ein Fehler";
    return -1;
}