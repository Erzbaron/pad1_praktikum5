#include "functions.h"

//-----------------------------------------------------------------------------
// 2 Variablen werden im Stack erstellt, ihre Adresse ausgegeben und
// getestet welche gr��er ist.

void stacktest() {
    int eins{};
    int zwei{};
    cout << "\nStack" << endl;
    cout << "-------------------------------------------------------------" << endl;
    cout << "Die erste Variable hat Adresse: " << &eins << endl;
    cout << "Die zweite Variable hat Adresse: " << &zwei << endl;

    if (&eins > & zwei)
        cout << "Die Adresse der ersten Variablen ist groesser." << endl;
    else
        cout << "Die Adresse der zweiten Variablen ist groesser." << endl;
}
//-----------------------------------------------------------------------------
// 2 Variablen werden im Heap erstellt, ihre Adresse ausgegeben und
// getestet welche gr��er ist.
void heaptest() {
    cout << "\nHeap" << endl;
    cout << "-------------------------------------------------------------" << endl;
    int* firstptr{ new int{} };
    int* secondptr{ new int{} };

    cout << "Der erste Pointer lautet: " << firstptr << endl;
    cout << "Der zweite Pointer lautet: " << secondptr << endl;

    if (firstptr > secondptr)
        cout << "Der erste Pointer ist groesser." << endl;
    else
        cout << "Der zweite Pointer ist groesser." << endl;

    delete firstptr;
    firstptr = nullptr;
    delete secondptr;
    secondptr = nullptr;
}

//-----------------------------------------------------------------------------
// Achtung, Stackoverflow wird hervorgerufen.

void heapoverflow() {

    cout << "\nHeapoverflow" << endl;
    cout << "-------------------------------------------------------------" << endl;

	int counter{};
	int* intptr{ nullptr };

	while (true) {

		intptr = new int[10000];
		counter += 40000;
		cout << counter << " ";

	}

}