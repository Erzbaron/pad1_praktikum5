/*
Pierre Baron 05.07.2020
PAD Praktikum 5
Aufgabe 3
Das Programm soll Stack und Heap Adressen ausgeben und dabei anzeigen, welche der Adressen groesser ist.
Die heapoverflow() Funktion allocated neuen Speicher in einer Endlosschleife bis es zum Crash kommt.
*/

#include "myError.h"
#include <iostream>
#include "functions.h"

using std::cout;
using std::endl;








int main()try
{

    stacktest();
    // Stack erweitert sich nach unten.
    heaptest();
    // Heap erweitert sich grundsätzlich nach oben.
    // Es kann vorkommen, dass vom Betriebssystem in der Zwischenzeit Daten
    // im Heap freigegeben werden und dadurch eine Adresse unter der
    // anderen liegt.

   // heapoverflow();
    // Ging bis 2,12156 GB
    // Danach Bad Allocation Fehler
}




catch (std::exception& e)
{
    std::cerr << "Fehler: " << e.what();
    return -2;
}
catch (...)
{
    std::cerr << "Ein Fehler";
    return -1;
}