#pragma once
#include <vector>
#include <string>
#include <iostream>
#include "Auftrag.h"
using std::vector;
using std::string;
using std::cout;
using std::endl;


class Auftrag;
class Kunde
{
public:
	Kunde();
	Kunde(string s);
	void print() const;
	string getName()const;

	// Assoziations-Operationen:
	void addAuftrag(Auftrag*);
	vector <Auftrag*> getAuftrage() const;

	void addAuftragvonKundeaufgerufen(Auftrag*);

private:
	string name;
	//mehrere Assoziationszeiger:
	vector<Auftrag*> vpa;

};