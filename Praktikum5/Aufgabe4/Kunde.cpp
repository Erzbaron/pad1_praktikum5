#include "Kunde.h"
//---------------------------------------------------------------------------------------------
//Konstruktoren

Kunde::Kunde() : name{ "noch nicht angelegt" }, vpa{}{}

Kunde::Kunde(string s) : name{s}, vpa{}{}

//-------------------------------------------------------------------------------------
// print


void Kunde::print()const {
	cout << endl;
	if (vpa.size() == 0)
		cout << "Kunde: " << name << " hat keine Auftraege" << endl <<endl;
	else {
		cout << "Kunde: " << name << " hat folgende Auftraege: " << endl;
		for (unsigned int i{}; vpa.size() > i; ++i) {
			//(this->getAuftrage().at(i))->print();
			vpa.at(i)->print();
		
		}
		cout << endl;
	}
}

//---------------------------------------------------------------------------------------------
// getter und setter

string Kunde::getName() const {
	return name;
}

void Kunde::addAuftrag(Auftrag* a) {
	vpa.push_back(a);
	a->addKundevonAuftragaufgerufen(this);
}

vector<Auftrag*> Kunde::getAuftrage() const {
	return vpa;
}

//--------------------------------------------------------------------------------
//Funktion die nur von der anderen Klasse aufgerufen wird.

void Kunde::addAuftragvonKundeaufgerufen(Auftrag* vonKunde) {
	vpa.push_back(vonKunde);
}