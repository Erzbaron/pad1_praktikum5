/*
Pierre Baron 05.07.2020
PAD 1 Praktikum 5
Aufgabe 4
Kunde und Auftrag Quellcode aus der Vorlesung zum laufen bringen.
*/


#include "myError.h"
#include <iostream>
#include "Auftrag.h"
#include "Kunde.h"
#include "funktionen.h"


int main()try
{
    testaufgabenstellung();

    //runtest();

    return 0;
}




catch (std::exception& e)
{
    std::cerr << "Fehler: " << e.what();
    return -2;
}
catch (...)
{
    std::cerr << "Ein Fehler";
    return -1;
}