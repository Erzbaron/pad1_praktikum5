#include "funktionen.h"

//-----------------------------------------------------------------------------
// Testcode aus der Aufgabenstellung

void testaufgabenstellung() {

    // Anlegen: zwei Kunden und drei Auftraege
    Kunde* k1{ new Kunde { "Sepplhuber-Finsterwalder" } };
    Kunde k2{ "Kurz" };
    Auftrag* a1{ new Auftrag { "Decke streichen" } };
    Auftrag* a2{ new Auftrag { "Wand tapezieren" } };
    Auftrag a3{ "Jalousie montieren" };
    Auftrag a4{ "Laminat verlegen" };
    // Erste Assoziationsrichtung:
    // aus einem Objekt vom Typ Kunde
    // zu Objekten vom Typ Auftrag
    k1->addAuftrag(a1);
    k1->addAuftrag(a2);
    k1->addAuftrag(&a3);

    // Zweite Assoziationsrichtung: 
    // aus einem Objekt vom Typ Auftrag
    // zu einem Objekt vom Typ Kunde
    a4.setKunde(&k2);
    // Alles ausgeben:
    k1->print();
    k2.print();
    a1->print();
    a2->print();
    a3.print();
    a4.print();

    // Speicher zurueckgeben:
    delete k1; k1 = nullptr;
    delete a1; a1 = nullptr;
    delete a2; a2 = nullptr;

}
//-----------------------------------------------------------------------------
// Eine Testfunktion, die ein paar F�lle durch probiert.
void runtest() {
    cout << "runtest" << endl;
    cout << "---------------------------------------" << endl;
    Kunde kunde1{ "Hans" };
    Kunde* kunde2{ new Kunde{ "Peter" } };
    Auftrag auftrag1{ "putzen" };
    Auftrag* auftrag2{ new Auftrag{ "Bild aufh�ngen" } };
    kunde1.addAuftrag(&auftrag1);
    kunde1.addAuftrag(auftrag2);
    kunde1.print();
    kunde2->print();
    auftrag1.print();
    auftrag2->print();

    Kunde kunde3{ "Mayer" };
    Auftrag auftrag3{ "lehren" };
    auftrag3.setKunde(&kunde3);
    kunde3.print();


}