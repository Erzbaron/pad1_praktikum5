#include "Auftrag.h"

//-----------------------------------------------------------------------------
// Konstruktoren

Auftrag::Auftrag() : id{""}, pk{}{}

Auftrag::Auftrag(string s) : id{ s }, pk{}{}

//-----------------------------------------------------------------------------
// Print Funktion

void Auftrag::print() const {
	cout << "Auftrag: " << id << endl;
}

//-----------------------------------------------------------------------------
// getter und setter

string Auftrag::getID() const {
	return id;
}

void Auftrag::setKunde(Kunde* k) {
	pk = k;
	k->addAuftragvonKundeaufgerufen(this);

}

Kunde* Auftrag::getKunde() const {
	return pk;
}

//--------------------------------------------------------------------------------
//Funktion die nur von der anderen Klasse aufgerufen wird.

void Auftrag::addKundevonAuftragaufgerufen(Kunde* vonAuftrag) {
	pk = vonAuftrag;
}