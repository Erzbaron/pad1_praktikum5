#pragma once
#include <string>
#include <iostream>
#include "Kunde.h"
using std::string;
using std::cout;
using std::endl;

class Kunde;
class Auftrag
{
public:
	Auftrag();
	Auftrag(string s);
	void print() const;
	string getID() const;
	

	// Assoziations-Operationen:
	void setKunde(Kunde*);
	Kunde* getKunde() const;

	void addKundevonAuftragaufgerufen(Kunde*);

private:
	string id;
	// ein Assoziationszeiger:
	Kunde* pk;

};